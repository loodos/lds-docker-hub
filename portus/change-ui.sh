#!/bin/sh

set -x

# change title and meta content
sed -i -e 's/Portus/Loodos Docker Hub/g' /srv/Portus/app/views/layouts/application.html.slim
sed -i -e 's#Created with#Forked from \n                a href="https://github.com/SUSE/Portus" target="_blank" SUSE/Portus\n                '"'"'  with#g' /srv/Portus/app/views/layouts/application.html.slim
sed -i -e 's#by the SUSE team#by the \n                a href="https://loodos.com" target="_blank" Loodos\n                '"'"'  team#g' /srv/Portus/app/views/layouts/application.html.slim
sed -i -e 's/Portus/Loodos Docker Hub/g' /srv/Portus/app/views/layouts/authentication.html.slim
sed -i -e 's/Portus/Loodos Docker Hub/g' /srv/Portus/app/views/layouts/errors.html.slim

# inject customine.css before body tag
cssCustomize='link href="/customize.css" rel="stylesheet" media="all"'
if ! grep -q "$cssCustomize" /srv/Portus/app/views/layouts/application.html.slim; then
  sed -i -e "/body data-controller/i \   \ $cssCustomize\n\n" /srv/Portus/app/views/layouts/application.html.slim
fi
if ! grep -q "$cssCustomize" /srv/Portus/app/views/layouts/authentication.html.slim; then
  sed -i -e "/body.login/i \   \ $cssCustomize\n\n" /srv/Portus/app/views/layouts/authentication.html.slim
fi
if ! grep -q "$cssCustomize" /srv/Portus/app/views/layouts/errors.html.slim; then
  sed -i -e "/body.login/i \   \ $cssCustomize\n\n" /srv/Portus/app/views/layouts/errors.html.slim
fi
