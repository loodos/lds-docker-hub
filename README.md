# Loodos Docker Hub & Registry

This is yet another [SUSE/Portus](http://port.us.org/) fork with auto-generated ssl certificate and ui customization.

## Involved components/images:

- https://github.com/SUSE/Portus
- https://docs.docker.com/registry/
- https://nginx.org/en/
- https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion
- https://github.com/jwilder/docker-gen
- https://github.com/jsecchiero/letsencrypt-portus
- https://github.com/thpham/portus-registry-tls-compose
- https://mariadb.org/

## Usage

edit .env variable before deploy

- `PORTUS_FQDN` (HUB) and `REGISTRY_FQDN` (REGISTRY) must be reachable domain from internet to work
- LetsEncrypt email required for auto-generated ssl certificate
- You can define database password
- You can define portus admin password (admin username=portus)
- You can define portus email configuration
- You can define portus social media (google, bitbucket, gitlab) login

## Deployment

```
docker-compose up -d
```

After the deploy go to `PORTUS_FQDN`:

- create a user or login as admin
- edit _Name_ with `registry`
- edit _Hostname_ with `registry:5000`
- click on show advanced button
- edit _External Registry Name_ with `registry.example.com` (REGISTRY_FQDN)

## UI Customization

- You can override UI style from `portus/customize.css`
- You can change application logos from `portus/custom-assets` folder
- You can change title, meta and footer from `portus/change-ui.sh` script file

## Docker login

```
docker login -u <username> -p <password> registry.example.com
```

If your account not contains password you can generate application token from UI and use this instead of password for docker login.

## Docker push image

Before push an image you must to tag image like this:

```
REGISTRY_FQDN\<namespace>\<imagename>:<tag>
```

### tag examples:

```
docker tag myapp:latest registry.example.com\registry\myapp:latest
docker tag myapp:latest registry.example.com\myapp:latest -> using default/global namespace
docker tag myapp:4.2.9 registry.example.com\emrah.cetiner\myapp:4.2.9 -> using personel namespace
```

### push examples:

```
docker push registry.example.com\registry\myapp:latest registry.example.com
docker push registry.example.com\myapp:latest registry.example.com
docker push registry.example.com\emrah.cetiner\myapp:4.2.9 registry.example.com
```

### pull examples:

```
docker pull registry.example.com\registry\myapp:latest registry.example.com
docker pull registry.example.com\myapp:latest registry.example.com
docker pull registry.example.com\emrah.cetiner\myapp:4.2.9 registry.example.com
```
